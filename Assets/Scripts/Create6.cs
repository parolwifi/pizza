﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Create6 : MonoBehaviour
{
    public GameObject circle;
    public Vector3[] circle6;
    private GameObject[] circles = new GameObject[6];
    private void Start()
    {     
        for(int i=0; i<circle6.Length; i++)
        {
            circles[i] = Instantiate(circle, circle6[i], Quaternion.identity) as GameObject;
        }
    }
}
