﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Swap : MonoBehaviour
{
    public static int score;
    public int[] circleArr = new int[6];
    private GameObject Parent;
    private GameObject Sector;
    private int coll;
    private bool click, check;
    public void Start()
    {
        score = 0;
        check = true;
        click = true;
    }
    public void Update()
    {
        if(click == false && check == true)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (Fill.placeS[i] == 1 && circleArr[i] == 1)
                    {
                        check = false;
                    }
                }
            }
        }
    }
    public void OnMouseDown()
    {
        click = false;
        check = true;
    }
    public void OnMouseUpAsButton()
    {
        click = true;
        Sector = GameObject.FindGameObjectWithTag("Sector");
        for (int i = 0; i < 6; i++)
        {
            if (Fill.placeS[i] == 1)
            {
                circleArr[i] = 1;
            }
        }
        for (int i = 0; i < 6; i++)
        {   
            if (check == true)
            {
                Parent = gameObject;
                Sector.transform.SetParent(Parent.transform);
                Sector.transform.localPosition = Vector3.zero;
                Sector.gameObject.tag = "Untagged";
                for (int x = 0; x < 6; x++)
                {
                    Fill.placeS[x] = 0;
                }
                Fill.yes = false;
                check = true;
            }     
        }
        for (int i = 0; i < 6; i++)
        {
            if (circleArr[i] == 1)
            {
                coll++;
                if (coll == 6)
                {
                    foreach (Transform child in transform)
                    {
                        score += 10;
                        Destroy(child.gameObject);
                    }
                    for (int x = 0; x < 6; x++) circleArr[x] = 0;
                }
            }
        }
        coll = 0;
    }
}
