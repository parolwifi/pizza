﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class Fill : MonoBehaviour
{
    public static int[] placeS = new int[6];
    public static GameObject[] circleParent = new GameObject[6];
    public Canvas canvas;
    public Text youScore, hightScore;
    private int scor, record;
    public GameObject sector;
    public GameObject main;
    public static int k, p;
    public static bool yes = false;
    private bool clickForCheck = true;
    public void Start()
    {
        yes = false;
        circleParent = GameObject.FindGameObjectsWithTag("Parent");
        main = GameObject.FindGameObjectWithTag("Player");       
    }
    public void Update()
    {
        if (clickForCheck == false)
        {
            canvas.gameObject.SetActive(true);
        }
        if (yes == false)
        {
            if (gameObject == main)
            {
                int rnd = Random.Range(0, placeS.Length);
                for (int i = 0; i < 6; i++)
                {
                    if (i == rnd)
                    {
                        placeS[i] = 1;
                        p = i;
                    }
                }
                foreach (int x in placeS)
                {
                    if (x == 1 && yes == false)
                    {
                        if (rnd == 0) Instantiate(sector).gameObject.transform.Rotate(Vector3.forward * 120);
                        else if (rnd == 1) Instantiate(sector).gameObject.transform.Rotate(Vector3.forward * 60);
                        else if (rnd == 2) Instantiate(sector).gameObject.transform.Rotate(Vector3.forward * 0);
                        else if (rnd == 3) Instantiate(sector).gameObject.transform.Rotate(Vector3.forward * 300);
                        else if (rnd == 4) Instantiate(sector).gameObject.transform.Rotate(Vector3.forward * 240);
                        else if (rnd == 5) Instantiate(sector).gameObject.transform.Rotate(Vector3.forward * 180);
                        yes = true;
                    }                    
                }
            }
        }
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (circleParent[j].GetComponent<Swap>().circleArr[i] == 1)
                {
                    k++;
                    if (k == 6 && i == p)
                    {
                        clickForCheck = false;
                    }
                }
            }
            k = 0;
        }
        scor = Swap.score;
        if (scor > record)
        {
            PlayerPrefs.SetInt("SaveScore", scor);
            PlayerPrefs.Save();
        }
        record = PlayerPrefs.GetInt("SaveScore");
        youScore.text = System.Convert.ToString(scor);
        hightScore.text = System.Convert.ToString(record);
        if (Input.GetKeyUp(KeyCode.Q))
        {
            PlayerPrefs.DeleteAll();
        }
    } 
    public void Restart()
    {
        SceneManager.LoadScene("Main");
    }
}
 